package com.melon.otpsms.controller;

import com.melon.otpsms.helper.JdbcOracle;
import com.melon.otpsms.model.MtQueue;
import com.melon.otpsms.repo.MoRepository;
import com.melon.otpsms.repo.MtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.Date;

/**
 * Created by IT19 on 16/06/2017.
 */
@RestController
public class MoReceiverController {

    @Autowired
    private MoRepository moRepository;
    @Autowired
    private MtRepository mtRepository;
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String index(){
        return "SMS OTP Running";
    };
    private Integer idx;

    @RequestMapping(value = "/moReceiver",produces = "application/json",method = RequestMethod.POST)

    public Success moReceiver(
            @RequestParam(value="proxyMoTxId",required = false) String proxyMoTxId,
            @RequestParam(value="proxyHostName",required = false) String proxyHostName,
            @RequestParam(value="msisdn",required = true) String msisdn,
            @RequestParam(value="msg",required = true) String msg
    ) throws MissingServletRequestParameterException, ClassNotFoundException, SQLException {
            JdbcOracle ora = new JdbcOracle();
            idx = ora.getSeqMtQueue();
        MtQueue mtQueue = new MtQueue();
        mtQueue.setId(idx);
        mtQueue.setHostName(null);
        mtQueue.setMsg(msg);
        mtQueue.setMsgType(10);
        mtQueue.setMsisdn(msisdn);
        mtQueue.setReceiveDate(new Date());
        mtQueue.setRegDate(new Date());
        mtQueue.setRegTableName(null);
        mtQueue.setReqParam1(null);
        mtQueue.setReqParam2(null);
        mtQueue.setReqParam3(null);
        mtQueue.setResponseTid(null);
        mtQueue.setSdc(null);
        mtQueue.setSdMoTxId((long) 1234567);
        mtQueue.setStatusCd("QS0001");
        mtQueue.setTariffCd(0);
        mtQueue.setTelcoId(0);
        mtQueue.setTransmitDate(new Date());
        mtQueue.setUpdDate(new Date());
        mtQueue.setUserId((long) 0);
        mtRepository.save(mtQueue);


//
//            MoQueue moQueue;
//            Mo mo = new Mo();
//            moQueue = new MoQueue();
//            //moQueue.setId(id);
//            moQueue.setHostName("");
//            moQueue.setMsg(msg);
//            moQueue.setMsisdn(msisdn);
//            moQueue.setParam1("");
//            moQueue.setParam2("");
//            moQueue.setParam3("");
//            moQueue.setProxyHostName("");
//            moQueue.setProxyMoTxId("");
//            moQueue.setPurchaseReqId((long) 83647364);
//            moQueue.setRegDate(new Date());
//            moQueue.setStatusCd("QS0001");
//            moQueue.setTelcoId(null);
//            moQueue.setUpdDate(new Date());
//            moQueue.setUserId(1);
//
//            moRepository.save(moQueue);
//
//            System.out.println(moQueue.getId());
//            mo.status = 200;
            Success resp = new Success();
            resp.setCode("200");
            resp.setMessage(String.valueOf(idx));
//            long startProcTime =System.currentTimeMillis();
//            mo.responseTime = System.currentTimeMillis() - startProcTime;
        return resp;
    }
}
