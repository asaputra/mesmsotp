package com.melon.otpsms.controller;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IT19 on 15/06/2017.
 */
@RestController
public class SmsOtpController {
    @RequestMapping(value = "/melonOtp",produces = "application/json")
    public Success deliveryReport(
            @RequestParam(value="messageid",required = true) String messageId,
            @RequestParam(value="status",required = true) String status
    ) throws MissingServletRequestParameterException {
        try{
            return new Success("200","Success");
        }catch(Exception ex){
            return new Success("500",ex.getMessage());
        }
    }
}
