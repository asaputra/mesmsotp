package com.melon.otpsms.controller;

import java.util.Map;

public class ErrorJson extends BaseResponse{


    public String timeStamp;
    public String error;
    public String message;
    public String trace;

    public ErrorJson(int status, Map<String, Object> errorAttributes) {
        this.status = status;
        this.error = (String) errorAttributes.get("error");
        this.message = (String) errorAttributes.get("message");
        this.timeStamp = errorAttributes.get("timestamp").toString();
        this.trace = (String) errorAttributes.get("trace");
    }

}