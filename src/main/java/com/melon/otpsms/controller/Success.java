package com.melon.otpsms.controller;

public class Success {
    private String code;
    private String message;

    public Success() {
        this.setCode("");
        this.setMessage("Complete Successfully.");
    }

    public Success(String code, String message) {
        this.setCode(code);
        this.setMessage(message);
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public String toString() {
        return "SuccessModel [code=" + this.code + ", message=" + this.message + "]";
    }
}
