package com.melon.otpsms.scheduller;

import com.melon.otpsms.helper.JdbcOracle;
import com.melon.otpsms.model.MtQueue;
import com.melon.otpsms.model.MtQueueLog;
import com.melon.otpsms.remote.MdMediaClient;
import com.melon.otpsms.repo.MtLogRepository;
import com.melon.otpsms.repo.MtRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 16/06/2017.
 */

@Component
public class MtProcessor {
    @Autowired
    private MtRepository mtRepository;
    @Autowired
    private MdMediaClient mdMediaClient;
    @Autowired
    private MtLogRepository mtLogRepository;
   // @Autowired
    private JdbcOracle jdbcOracle;



    private static final Logger log = LoggerFactory.getLogger(MoGrabber.class);
    @Scheduled(fixedRate = 2000)
    public void Process() throws ClassNotFoundException, SQLException {
        log.info("------------- SMS OTP MT Running ------------");
        List<MtQueue> mtQueues = mtRepository.findProcessByStatus();
        int i =0;
        jdbcOracle = new JdbcOracle();
        for(MtQueue mtQueue:mtQueues){
            mtQueue.setStatusCd("QS0003");
            String sendSms = mdMediaClient.send(mtQueue);
            if(sendSms.length() > 60){
                sendSms = "FAILED|HTTP STATUS 500";
            }
            log.info("Send SMS result : " + sendSms);
            log.info("Send SMS result : " + sendSms);
            mtRepository.save(mtQueue);
            MtQueueLog mtLog = new MtQueueLog();
            mtLog.setStatusCd(mtQueue.getStatusCd());
            mtLog.setId(jdbcOracle.getSeqMtQueueLog());
            mtLog.setMtId(mtQueue.getId());
            mtLog.setSdMoTxId(mtQueue.getSdMoTxId());
            mtLog.setMsgType(mtQueue.getMsgType());
            mtLog.setMsg(mtQueue.getMsg());
            mtLog.setMsisdn(mtQueue.getMsisdn());
            mtLog.setReceiveDate(mtQueue.getRegDate());
            mtLog.setRegDate(new Date());
            mtLog.setResponseTid(sendSms);

            mtLogRepository.save(mtLog);
            mtRepository.delete(mtQueue);

            //insert log ke oracle
            jdbcOracle.InsertLog(mtLog);
        }
    }
}
