package com.melon.otpsms.scheduller;

import com.melon.otpsms.model.MoQueue;
import com.melon.otpsms.model.MtQueue;
import com.melon.otpsms.repo.MoRepository;
import com.melon.otpsms.repo.MtRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by IT19 on 16/06/2017.
 */
@Component
public class MoGrabber {
    @Autowired
    private MoRepository moRepository;

    @Autowired
    private MtRepository mtRepository;
    private static final Logger log = LoggerFactory.getLogger(MoGrabber.class);

    //@Scheduled(fixedRate = 2000)
    public void GrabberData(){
        log.info("TESTTT JOOOBBBB SSSS");
        MtQueue mtQueue;
        List<MoQueue> moQueues = moRepository.findProcessByStatus();
        int i =0;
        for(MoQueue moQueue:moQueues){
            mtQueue = new MtQueue();
            moQueue.setStatusCd("QS0003");
            log.info("Found ("+i+") : " + moQueue);
            mtQueue.setId(1093498);
            mtQueue.setHostName(null);
            mtQueue.setMsg(moQueue.getMsg());
            mtQueue.setMsgType(10);
            mtQueue.setMsisdn(moQueue.getMsisdn());
            mtQueue.setReceiveDate(new Date());
            mtQueue.setRegDate(new Date());
            mtQueue.setRegTableName(null);
            mtQueue.setReqParam1(null);
            mtQueue.setReqParam2(null);
            mtQueue.setReqParam3(null);
            mtQueue.setResponseTid(null);
            mtQueue.setSdc(null);
            mtQueue.setSdMoTxId(moQueue.getId());
            mtQueue.setStatusCd("QS0001");
            mtQueue.setTariffCd(0);
            mtQueue.setTelcoId(0);
            mtQueue.setTransmitDate(new Date());
            mtQueue.setUpdDate(new Date());
            mtQueue.setUserId((long) 0);

            moRepository.save(moQueue);
            mtRepository.save(mtQueue);
        }
    }
}
