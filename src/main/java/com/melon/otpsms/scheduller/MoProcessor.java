package com.melon.otpsms.scheduller;

/**
 * Created by IT19 on 16/06/2017.
 */

import com.melon.otpsms.controller.Success;
import com.melon.otpsms.helper.JdbcOracle;
import com.melon.otpsms.model.MoQueue;
import com.melon.otpsms.model.MoQueueLog;
import com.melon.otpsms.model.MtQueue;
import com.melon.otpsms.repo.MoLogRepository;
import com.melon.otpsms.repo.MoRepository;
import com.melon.otpsms.repo.MtRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Component
public class MoProcessor {

    @Value("${node.id}")
    private String hostName;

    @Value("${mapi.client.url}")
    private String host;

    @Autowired
    private MoRepository moRepository;

    @Autowired
    private MtRepository mtRepository;

    @Autowired
    private MoLogRepository moLogRepository;

    private static final Logger log = LoggerFactory.getLogger(MoProcessor.class);

    //private MapiClient mapiClient = new MapiClient();

    //@Scheduled(fixedRate = 1000)
    public void reportCurrentTime() throws ClassNotFoundException, SQLException {
        log.info("******** SMS OTP MO Running *********");
        MtQueue mtQueue;
        List<MoQueue> moQueues = moRepository.findProcessByStatus();
        int i =0;
        JdbcOracle ora = new JdbcOracle();

        for(MoQueue moQueue:moQueues){
            //MapiClient mapiClient = new MapiClient();
//            MultiValueMap<String, String> param = new LinkedMultiValueMap<String, String>();
//            param.add("msisdn",moQueue.getMsisdn());
            Success respClient = null;
            //respClient = mapiClient.postForObject(host+"/verify/msisdnotp",param,Success.class);
//            respClient = mapiClient.getForObject("http://192.168.10.22:8000/mapi/verify/msisdnotp?msisdn="+moQueue.getMsisdn()+"&" +
//                    "_CPASS=DA910300C0761C36E041237F01990E33&_CNAME=LangitMusik MelOn Client&_DIR=c",Success.class);
//            //String respMapi = x.getBody();
//            log.info(respClient.getMessage());
            mtQueue = new MtQueue();
            mtQueue.setId(ora.getSeqMtQueue());
            moQueue.setStatusCd("QS0003");
            log.info("Found ("+i+") : " + moQueue);
            mtQueue.setHostName(null);
            mtQueue.setMsg(moQueue.getMsg());
            mtQueue.setMsgType(10);
            mtQueue.setMsisdn(moQueue.getMsisdn());
            mtQueue.setReceiveDate(new Date());
            mtQueue.setRegDate(new Date());
            mtQueue.setRegTableName(null);
            mtQueue.setReqParam1(null);
            mtQueue.setReqParam2(null);
            mtQueue.setReqParam3(null);
            mtQueue.setResponseTid(null);
            mtQueue.setSdc(null);
            mtQueue.setSdMoTxId(moQueue.getId());
            mtQueue.setStatusCd("QS0001");
            mtQueue.setTariffCd(0);
            mtQueue.setTelcoId(0);
            mtQueue.setTransmitDate(new Date());
            mtQueue.setUpdDate(new Date());
            mtQueue.setUserId((long) 0);

            MoQueueLog moLog = new MoQueueLog();
            moLog.setMoId(moQueue.getId());
            moLog.setMsisdn(moQueue.getMsisdn());
            moLog.setMsg(moQueue.getMsg());
            moLog.setMoRegDate(moQueue.getRegDate());
            moLog.setRegDate(new Date());
            moLog.setPurchaseReqId(moQueue.getPurchaseReqId());
            moLog.setStatusCd(moQueue.getStatusCd());
            moRepository.save(moQueue);
            mtRepository.save(mtQueue);

            moRepository.delete(moQueues);
            moLogRepository.save(moLog);

        }
    }
}
