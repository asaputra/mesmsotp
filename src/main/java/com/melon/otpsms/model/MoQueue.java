package com.melon.otpsms.model;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by IT19 on 16/06/2017.
 */
@Entity
@Table(name="SMSD_MO_QUEUE")

@GenericGenerator(
        name = "hilo_sequence_generator",
        strategy = "enhanced-table",
        parameters = {
                @Parameter(name = "table_name", value = "SEQ_ID"),
                @Parameter(name = "value_column_name", value = "NEXT_ID"),
                @Parameter(name = "segment_column_name", value = "SEQ_NAME"),
                @Parameter(name = "segment_value", value = "SMSD_MO_QUEUE_ID")
        }
)

@NamedNativeQueries({
        @NamedNativeQuery(name = "MoQueue.findUpdateByStatus",query = "SELECT * FROM SMSD_MO_QUEUE " +
                "WHERE STATUS_CD='QS0002' LIMIT 1",
                resultClass = MoQueue.class),
        @NamedNativeQuery(name = "MoQueue.findProcessByStatus",query = "SELECT * FROM SMSD_MO_QUEUE " +
                "WHERE STATUS_CD='QS0001' LIMIT 1",
                resultClass = MoQueue.class)
})

public class MoQueue extends BaseMo{
    @Id
    @GeneratedValue(generator = "hilo_sequence_generator")
    @Column(name="SD_MO_TX_ID")
    private Long id;

    @Column(name="MSISDN")
    private String msisdn;

    @Column(name="USER_ID")
    private long userId;

    @Column(name="TELCO_ID")
    private Integer telcoId;

    @Column(name="HOST_NAME")
    private String hostName;

    @Column(name="MSG")
    private String msg;

    @Column(name="PARAM1")
    private String param1;

    @Column(name="PARAM2")
    private String param2;

    @Column(name="PARAM3")
    private String param3;

    @Column(name="STATUS_CD")
    private String statusCd;

    @Column(name="REG_DATE")
    private Date regDate;

    @Column(name="UPD_DATE")
    private Date updDate;

    @Column(name="PROXY_MO_TX_ID")
    private String proxyMoTxId;

    @Column(name="PROXY_HOST_NAME")
    private String proxyHostName;

    @Column(name="PURCHASE_REQ_ID")
    private Long purchaseReqId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Integer getTelcoId() {
        return telcoId;
    }

    public void setTelcoId(Integer telcoId) {
        this.telcoId = telcoId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public String getParam2() {
        return param2;
    }

    public void setParam2(String param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }

    public String getProxyMoTxId() {
        return proxyMoTxId;
    }

    public void setProxyMoTxId(String proxyMoTxId) {
        this.proxyMoTxId = proxyMoTxId;
    }

    public String getProxyHostName() {
        return proxyHostName;
    }

    public void setProxyHostName(String proxyHostName) {
        this.proxyHostName = proxyHostName;
    }

    public Long getPurchaseReqId() {
        return purchaseReqId;
    }

    public void setPurchaseReqId(Long purchaseReqId) {
        this.purchaseReqId = purchaseReqId;
    }

    @Override
    public String toString() {
        return "MoQueue{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", userId=" + userId +
                ", telcoId=" + telcoId +
                ", hostName='" + hostName + '\'' +
                ", msg='" + msg + '\'' +
                ", param1='" + param1 + '\'' +
                ", param2='" + param2 + '\'' +
                ", param3='" + param3 + '\'' +
                ", statusCd='" + statusCd + '\'' +
                ", regDate=" + regDate +
                ", updDate=" + updDate +
                ", proxyMoTxId='" + proxyMoTxId + '\'' +
                ", proxyHostName='" + proxyHostName + '\'' +
                ", purchaseReqId=" + purchaseReqId +
                '}';
    }
}
