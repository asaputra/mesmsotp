package com.melon.otpsms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by IT19 on 03/07/2017.
 */

@Entity
@Table(name="SMSD_MT_QUEUE_LOG")

public class MtQueueLog {
    @Id
    @Column(name="SD_MT_TX_LOG_ID")
    private Integer id;

    @Column(name="SD_MT_TX_ID")
    private Integer mtId;

    @Column(name = "SD_MO_TX_ID")
    private Long sdMoTxId;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "MSISDN")
    private String msisdn;

    @Column(name = "HOST_NAME")
    private String hostName;

    @Column(name = "REQ_TABLE")
    private String regTableName;

    @Column(name = "REQ_PARAM1")
    private String reqParam1;

    @Column(name = "REQ_PARAM2")
    private String reqParam2;

    @Column(name = "REQ_PARAM3")
    private String reqParam3;

    @Column(name = "TELCO_ID")
    private int telcoId;

    @Column(name = "TARIFF_CD")
    private int tariffCd;

    @Column(name = "SDC")
    private String sdc;

    @Column(name = "MSG_TYPE")
    private Integer msgType;

    @Column(name = "MSG")
    private String msg;

    @Column(name = "IF_UID")
    private String responseTid;

    @Column(name = "STATUS_CD")
    private String statusCd;

    @Column(name = "REG_DATE")
    private Date regDate;

    @Column(name = "UPD_DATE")
    private Date updDate;

    @Column(name = "TRANSMIT_DATE")
    private Date transmitDate;

    @Column(name = "RECEIVE_DATE")
    private Date receiveDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMtId() {
        return mtId;
    }

    public void setMtId(Integer mtId) {
        this.mtId = mtId;
    }

    public Long getSdMoTxId() {
        return sdMoTxId;
    }

    public void setSdMoTxId(Long sdMoTxId) {
        this.sdMoTxId = sdMoTxId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getRegTableName() {
        return regTableName;
    }

    public void setRegTableName(String regTableName) {
        this.regTableName = regTableName;
    }

    public String getReqParam1() {
        return reqParam1;
    }

    public void setReqParam1(String reqParam1) {
        this.reqParam1 = reqParam1;
    }

    public String getReqParam2() {
        return reqParam2;
    }

    public void setReqParam2(String reqParam2) {
        this.reqParam2 = reqParam2;
    }

    public String getReqParam3() {
        return reqParam3;
    }

    public void setReqParam3(String reqParam3) {
        this.reqParam3 = reqParam3;
    }

    public int getTelcoId() {
        return telcoId;
    }

    public void setTelcoId(int telcoId) {
        this.telcoId = telcoId;
    }

    public int getTariffCd() {
        return tariffCd;
    }

    public void setTariffCd(int tariffCd) {
        this.tariffCd = tariffCd;
    }

    public String getSdc() {
        return sdc;
    }

    public void setSdc(String sdc) {
        this.sdc = sdc;
    }

    public Integer getMsgType(Integer msgType) {
        return this.msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResponseTid() {
        return responseTid;
    }

    public void setResponseTid(String responseTid) {
        this.responseTid = responseTid;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getUpdDate() {
        return updDate;
    }

    public void setUpdDate(Date updDate) {
        this.updDate = updDate;
    }

    public Date getTransmitDate() {
        return transmitDate;
    }

    public void setTransmitDate(Date transmitDate) {
        this.transmitDate = transmitDate;
    }

    public Date getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Date receiveDate) {
        this.receiveDate = receiveDate;
    }

    @Override
    public String toString() {
        return "MtQueueLog{" +
                "id=" + id +
                ", mtId=" + mtId +
                ", sdMoTxId=" + sdMoTxId +
                ", userId=" + userId +
                ", msisdn='" + msisdn + '\'' +
                ", hostName='" + hostName + '\'' +
                ", regTableName='" + regTableName + '\'' +
                ", reqParam1='" + reqParam1 + '\'' +
                ", reqParam2='" + reqParam2 + '\'' +
                ", reqParam3='" + reqParam3 + '\'' +
                ", telcoId=" + telcoId +
                ", tariffCd=" + tariffCd +
                ", sdc='" + sdc + '\'' +
                ", msgType=" + msgType +
                ", msg='" + msg + '\'' +
                ", responseTid='" + responseTid + '\'' +
                ", statusCd='" + statusCd + '\'' +
                ", regDate=" + regDate +
                ", updDate=" + updDate +
                ", transmitDate=" + transmitDate +
                ", receiveDate=" + receiveDate +
                '}';
    }
}
