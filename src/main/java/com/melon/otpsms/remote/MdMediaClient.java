package com.melon.otpsms.remote;

import com.melon.otpsms.helper.SimpleHttp;
import com.melon.otpsms.model.MtQueue;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by IT19 on 16/06/2017.
 */
@Component
public class MdMediaClient {
    @Value("${otp.sms.url}")
    private String mtUrl;

    @Value("${otp.sms.username}")
    private String userName;

    @Value("${otp.sms.password}")
    private String password;

    @Value("${otp.sms.sender_id}")
    private String senderId;

    @Autowired
    private SimpleHttp httpClient;

    Logger log = Logger.getLogger(MdMediaClient.class);

    private String convertMsisdn(String toConvert) {
        if (toConvert.charAt(0) == '0') {
            return "62" + toConvert.substring(1);
        }
        return toConvert;    // return unmodified string
    }

    public String send(MtQueue mt) {

        if (mt == null) {
            throw new IllegalStateException("MdMedia cannot be null");
        }
        log.info("============== MD Media OTP Sending =============");
        log.info("OTP target url=" + mtUrl);
        log.info("Sending OTPMt=" + mt);
        HashMap<String, String> parameters = convertToParameterMap(mt);
        log.info("MT Params : "+ parameters);
        return httpClient.get(mtUrl, parameters);
    }

    private HashMap<String, String> convertToParameterMap(MtQueue mt) {
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("user", userName);
        p.put("password", password);
        p.put("message",mt.getMsg());
        p.put("msisdn",mt.getMsisdn());
        p.put("senderid",senderId);

        return p;
    }

}
