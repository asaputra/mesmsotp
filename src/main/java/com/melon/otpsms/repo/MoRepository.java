package com.melon.otpsms.repo;

import com.melon.otpsms.model.MoQueue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IT19 on 16/06/2017.
 */
public interface MoRepository extends CrudRepository<MoQueue,Long> {
    @Query(nativeQuery=true)
    public MoQueue findUpdateByStatus();

    @Query(nativeQuery=true)
    public List<MoQueue> findProcessByStatus();

    //public void deleteAllByStatusCdAndHostName(String statusCd,String hostName);
}
