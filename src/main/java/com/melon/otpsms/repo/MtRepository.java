package com.melon.otpsms.repo;

import com.melon.otpsms.model.MtQueue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IT19 on 16/06/2017.
 */
public interface MtRepository extends CrudRepository<MtQueue,Long>{
    @Query(nativeQuery=true)
    public MtQueue findUpdateByStatus();

    @Query(nativeQuery=true)
    public List<MtQueue> findProcessByStatus();

    public void deleteAllByStatusCdAndHostName(String statusCd,String hostName);
}
