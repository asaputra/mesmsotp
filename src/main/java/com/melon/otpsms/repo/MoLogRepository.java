package com.melon.otpsms.repo;

import com.melon.otpsms.model.MoQueueLog;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IT19 on 25/07/2017.
 */
public interface MoLogRepository extends CrudRepository<MoQueueLog,Long>
{

}
