package com.melon.otpsms.helper;

import com.melon.otpsms.model.MtQueueLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IT19 on 23/08/2017.
 */
@Component
public class JdbcOracle {
    private Connection con ;
    private Statement stmt;
    private ResultSet rs;

    private static final Logger log = LoggerFactory.getLogger(JdbcOracle.class);

    public JdbcOracle() throws ClassNotFoundException, SQLException {
        con = DataSource.getConnection();
//        try {
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        con = null;
//        try {
//            con = DriverManager.getConnection(
//                    "jdbc:oracle:thin:@(description=(address_list=" +
//                            "(address=(host=192.168.10.40)(protocol=tcp)(port=1521)) " +
//                            "(address=(host=192.168.10.37)(protocol=tcp)(port=1521)) " +
//                            "(load_balance=yes)(failover=yes))(connect_data=(service_name=MelOndb)))", "MBP001", "51tiB4dr14H111191");
//            //System.out.println("cOnnect");
//        }catch (Exception ex){
//            //System.out.println("Not cOnnect");
//            log.info(ex.getMessage());
//        }

    }

    public void UpdateUserPass(){

    }

    public Integer getSeqMtQueue() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select smsd_mt_queue_seq.nextval as mtTxId from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);

        }

        return rslt;
    }

    public Integer getSeqMtQueueLog() throws SQLException {
        stmt = null;
        ResultSet rs = null;
        stmt = con.createStatement();
        Integer rslt = null;
        String sql = "select smsd_mt_queue_log_seq.nextval as mtTxId from dual ";
        rs =stmt.executeQuery(sql);
        if(rs.next()){
            rslt = rs.getInt(1);

        }
        stmt.close();
        return rslt;
    }

    public void InsertLog(MtQueueLog mt){

        stmt = null;
        rs = null;
        String [] r = mt.getResponseTid().split("\\|");
        String rsltCode = "1";
        if(r[0].equals("FAILED")){
            rsltCode = "0";
        }else{
            rsltCode = "1";
        }
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String RegDate = df.format(new Date());
        String ReceiveDate = df.format(new Date());
        System.out.println(RegDate);
        try{
            stmt = con.createStatement();
            String sql = "insert into MBP002.SMSD_MT_QUEUE_LOG(sd_mt_tx_log_id," +
                    "sd_mt_tx_id," +
                    "sd_mo_tx_id," +
                    "msisdn," +
                    "msg," +
                    "result_code," +
                    "status_cd," +
                    "reg_date," +
                    "receive_date," +
                    "telco_id," +
                    "msg_type," +
                    "tariff_cd," +
                    "sdc," +
                    "req_table," +
                    "host_name" +
                    ") values (\n" +
                    "MBP002.SMSD_MT_QUEUE_LOG_SEQ.NEXTVAL,\n" +
                    mt.getMtId()+",\n" +
                    mt.getSdMoTxId()+",\n" +
                    "'"+mt.getMsisdn()+"',\n" +
                    "'"+mt.getMsg()+"',\n" +
                    "'"+rsltCode+"',\n" +
                    "'"+mt.getStatusCd()+"',\n" +
                    "to_date('"+RegDate+"','dd-mm-yyyy'),\n" +
                    "to_date('"+ReceiveDate+"','dd-mm-yyyy')," +
                    "get_telco_id_by_msisdn('"+mt.getMsisdn()+"'),\n" +
                    "0,\n" +
                    "'73',\n" +
                    "'MDM',\n" +
                    "'MS_AUTH_SMS',\n" +
                    "'INTERFACE1'\n" +
                    ") ";
            //System.out.println(sql);
            stmt.executeQuery(sql);
            stmt.close();
        }catch(Exception ex){
            log.info(ex.getMessage());
            //System.out.println(ex.getMessage());
        }
    }
}
