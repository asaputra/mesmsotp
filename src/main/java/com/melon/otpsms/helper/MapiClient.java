/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.melon.otpsms.helper;

import com.sun.net.httpserver.Authenticator.Failure;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.Assert;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.*;

import javax.naming.AuthenticationException;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IT19
 */

public class MapiClient extends RestTemplate {
    
//    private HttpHeaders headers = new HttpHeaders();
//    private RestTemplate restTemplate;
//    private ClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
//    public String urL = "http://192.168.10.31:8000/mapi";
//    private String clientAdmin = "MelonBox Admin";
//    private String adminPass = "YDL6AE0DDA8C03MELN0001200D387J2G";
    
    private HttpHeaders headers = new HttpHeaders();
    //private String host = "http://localhost:8080/mapi";
    private String host = "http://192.168.10.28:8000/mapi";
    //private Logger logger = Logger.getLogger(MapiRestTemplate.class);

    @Deprecated
    protected String authenticationKey;

//    private String clientName = "LangitMusik MelOn Client";
//    private String password = "DA910300C0761C36E041237F01990E33";

    private String clientName = "LangitMusik MelOn Client";
    private String password = "DA910300C0761C36E041237F01990E33";

    public MapiClient(){

        setErrorHandler(new ResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                HttpStatus statusCode = response.getStatusCode();
                return !statusCode.equals(HttpStatus.OK);
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

                //ì—ëŸ¬ í•¸ë“¤ë§
                HttpStatus statusCode = response.getStatusCode();

//                if (statusCode.equals(HttpStatus.UNAUTHORIZED))
//                    throw new AuthorityException();

                if (statusCode.equals(HttpStatus.FORBIDDEN))
                    try {
                        throw new AuthenticationException();

                } catch (AuthenticationException ex) {
                    Logger.getLogger(MapiClient.class.getName()).log(Level.SEVERE, null, ex);
                }

                HttpMessageConverterExtractor<Failure> responseErrorExtractor =
                        new HttpMessageConverterExtractor<Failure>(Failure.class, getMessageConverters());

                try {
                    Failure extractData = responseErrorExtractor.extractData(response);
                    //throw new MethodFailureException(extractData.getCode(), extractData.getMessage());
                } catch (RuntimeException re) {
//                    if (re instanceof MethodFailureException)
//                        throw re;
                    HttpMessageConverterExtractor<String> extractor =
                            new HttpMessageConverterExtractor<String>(String.class, getMessageConverters());
                    String extractData = extractor.extractData(response);
                    //throw new FailureException(ErrorCode.EC_INTERNAL_SERVER_ERROR, extractData);
                }

            }
        });
    }
     public HttpHeaders getRequestHttpHeaders() {
        return headers;
    }

    @SuppressWarnings("unchecked")
    public <T> T deleteWithResponse(String url, Object request, Class<T> responseType, Map<String, ?> uriVariables)
            throws RestClientException {

        if (request instanceof MultiValueMap) {
            MultiValueMap<String, String> m = (MultiValueMap<String, String>) request;
            m.add("_method", "DELETE");
        }

        return this.postForObject(url, request, responseType, uriVariables);
    }

    protected <T> T doExecute(URI url, HttpMethod method, RequestCallback requestCallback,
                              ResponseExtractor<T> responseExtractor) throws RestClientException {

        Assert.notNull(url, "'url' must not be null");
        Assert.notNull(method, "'method' must not be null");
        ClientHttpResponse response = null;

        try {
            int i = 0;
            while (i++ < 2) {
                response = null;
                ClientHttpRequest request = createRequest(url, method);
                request.getHeaders().putAll(headers);

                if (requestCallback != null) {
                    requestCallback.doWithRequest(request);
                }

//                List<String> values = new ArrayList<String>();
//                values.add("application/xml");
//                values.add("text/html");
//                request.getHeaders().put("Accept", values);

                response = request.execute();
                logResponseStatus(method, url, response);

                if (!getErrorHandler().hasError(response)) {
                    //logger.debug("Response has error");
                    // handleResponseError(method, url, response);
                    break;
                }

                HttpStatus statusCode = response.getStatusCode();
                if (HttpStatus.FORBIDDEN.equals(statusCode)) {
                    ResponseEntity<MapiClient> authenticateClient = authenticateClient();
                    if (authenticateClient != null) {
                        logger.debug("authenticateClient.getBody() : " + authenticateClient.getBody());
                        logger.debug("authenticateClient.getHeaders() : " + authenticateClient.getHeaders().get("Set-Cookie"));
                        HttpHeaders headers2 = authenticateClient.getHeaders();
                        List<String> list = headers2.get("Set-Cookie");
                        headers.put("Cookie", list);
                        continue;
                    }
                }
                handleResponseError(method, url, response);
            }

            if (responseExtractor != null) {
                return responseExtractor.extractData(response);
            } else {
                return null;
            }
        } catch (IOException ex) {
            throw new ResourceAccessException("I/O error: " + ex.getMessage(), ex);
        } finally {
            if (response != null) {
                response.close();
            }
        }

    }
    private ResponseEntity<MapiClient> authenticateClient() {

        System.out.println("Called authenticateClient() method.");

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("clientName", clientName);
        map.add("password", password);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<MapiClient> postForEntity = null;
        try {
            postForEntity = restTemplate.postForEntity(host + "/authentication/client", map, MapiClient.class);
            HttpHeaders headers2 = postForEntity.getHeaders();
                        List<String> list = headers2.get("Set-Cookie");
                        System.out.println(list.get(0));
                        headers.put("Cookie", list);
        } catch (RestClientException rce) {
            logger.error(rce);
            rce.printStackTrace();
        }

        return postForEntity;
    }

    private void logResponseStatus(HttpMethod method, URI url,
                                   ClientHttpResponse response) {
        if (logger.isDebugEnabled()) {
            try {
                System.out.println(method.name() + " request for \"" + url
                        + "\" resulted in " + response.getStatusCode() + " ("
                        + response.getStatusText() + ")");
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    private void handleResponseError(HttpMethod method, URI url,
                                     ClientHttpResponse response) throws IOException {

        try {
            logger.warn(method.name() + " request for \"" + url
                    + "\" resulted in " + response.getStatusCode() + " ("
                    + response.getStatusText()
                    + "); invoking error handler");
        } catch (IOException e) {
            logger.error(e);
        }

        getErrorHandler().handleError(response);
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host;
    }

    @Deprecated
    public void setAuthenticationKey(String authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
