package com.melon.otpsms.helper;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by MelOnDev1 on 25/05/2017.
 */
@Component
public class SimpleHttp {
    private static final int DEFAULT_TIMEOUT = 30000; // ms
    private HttpClient client;
    private Logger log = Logger.getLogger(SimpleHttp.class);
    private int statusCode;

    public SimpleHttp() {
        client = new HttpClient();
        client.getParams().setParameter(HttpMethodParams.USER_AGENT,
                "MelON Mt Sender)");
        client.getHttpConnectionManager().getParams().setConnectionTimeout(DEFAULT_TIMEOUT);
        client.getHttpConnectionManager().getParams().setSoTimeout(DEFAULT_TIMEOUT);
        log = Logger.getLogger(SimpleHttp.class);
    }

    public SimpleHttp(int timeout) {
        MultiThreadedHttpConnectionManager connectionManager =
                new MultiThreadedHttpConnectionManager();
        connectionManager.getParams().setConnectionTimeout(timeout);
        connectionManager.getParams().setSoTimeout(timeout);
        connectionManager.getParams().setParameter(HttpMethodParams.USER_AGENT,
                "MelON Mt Sender)");
        client = new HttpClient(connectionManager);
//        client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
        log = Logger.getLogger(SimpleHttp.class);
    }

    public String get(String uri) {
        return get(uri, null);
    }

    public String get(String uri, HashMap<String, String> parameters) {
        GetMethod getMethod = new GetMethod(uri);
        return execute(getMethod, parameters);
    }

    public String post(String uri, HashMap<String, String> parameters) {
        PostMethod postMethod = new PostMethod(uri);
        return execute(postMethod, parameters);
    }

    private String execute(HttpMethodBase method, HashMap<String, String> parameters) {
        String returnVal = null;
        if (parameters != null) {
            method.setQueryString(convertNameValue(parameters));
        }

        try {
            log.debug("Execute HTTP method to URL=" + method.getURI());
            statusCode = client.executeMethod(method);
            if (statusCode == HttpStatus.SC_OK) {
                returnVal = read(method);
            } else {
                log.debug("Status code=" + statusCode);
                returnVal = read(method);
            }
        } catch (IOException e) {
            log.error(e.toString());
        } finally {
            method.releaseConnection();
        }
        return returnVal;
    }

    public String post(String uri) {
        return post(uri, null);
    }


    public String postXml(String uri) {
        return post(uri, null);
    }

    public String postXml(String uri, String xml, String authKey) {
        PostMethod postMethod = new PostMethod(uri);
        try {
            RequestEntity entity = new StringRequestEntity(xml, "text/xml", null);
            postMethod.setRequestEntity(entity);
            log.debug("content type=" + entity.getContentType());

            if (authKey != null) {
                Base64 base64 = new Base64();
                String encodedAuth = new String(base64.encode(authKey.getBytes()));
                String authorizationString = "Basic " + encodedAuth.trim();
                postMethod.setRequestHeader("Authorization", authorizationString);
            }
            postMethod.setRequestHeader("Content-Type", "text/xml");
            postMethod.setRequestHeader("Content-Length", String.valueOf(xml.length()));
//            NameValuePair[] pair = {new NameValuePair("xml", xml)};
//
//            postMethod.setRequestBody(pair);

        } catch (Exception e) {
            log.debug("Exception" + e);

        }
        try {
            statusCode = client.executeMethod(postMethod);
            log.debug("StatusCode=" + statusCode);
            return read(postMethod);
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            postMethod.releaseConnection();
        }
        return null;
//        return execute(postMethod, null);
    }


    private NameValuePair[] convertNameValue(Map params) {
        if (params == null) return null;

        NameValuePair[] rtPair = new NameValuePair[params.size()];

        Iterator it = params.entrySet().iterator();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            NameValuePair pair = new NameValuePair();
            String key = entry.getKey().toString();
            Object valueObj = entry.getValue();
            if (valueObj == null) {
                log.warn("Key " + key + " has NULL value, therefore will not be added to parameter map");

                String value = "";
                pair.setName(entry.getKey().toString());
                pair.setValue(value);
                rtPair[i] = pair;

            } else {
                String value = entry.getValue().toString();
                pair.setName(entry.getKey().toString());
                pair.setValue(entry.getValue().toString());
                rtPair[i] = pair;
            }
            i++;
        }
        return rtPair;
    }

    public int getStatusCode() {
        return statusCode;
    }

    private String read(HttpMethod method) throws IOException {
        InputStream in = method.getResponseBodyAsStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuffer sb = new StringBuffer(1024 * 4);
        String line;
        try {
            while (true) {
                line = br.readLine();
                if (line == null)
                    break;

                if (sb.length() > 0)
                    sb.append("\r\n");

                sb.append(line);
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }
}