package com.melon.otpsms.helper;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    static {
        config.setJdbcUrl( "jdbc:oracle:thin:@(description=(address_list=" +
                "(address=(host=192.168.10.40)(protocol=tcp)(port=1521)) " +
                "(address=(host=192.168.10.37)(protocol=tcp)(port=1521)) " +
                "(load_balance=yes)(failover=yes))(connect_data=(service_name=MelOndb)))");
        config.setUsername( "MBP001" );
        config.setPassword( "51tiB4dr14H111191" );
        config.setMaximumPoolSize(10);
        config.setDriverClassName("oracle.jdbc.OracleDriver");
        config.setJdbc4ConnectionTest(false);
        config.setConnectionTestQuery("SELECT 1 FROM DUAL");
        ds = new HikariDataSource( config );
    }

    private DataSource() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

}