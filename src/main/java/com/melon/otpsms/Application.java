package com.melon.otpsms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.sql.SQLException;

/**
 * Created by IT19 on 15/06/2017.
 */
@SpringBootApplication
@EnableScheduling
public class Application {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        SpringApplication.run(Application.class, args);

        //Untuk tes ke oracle insert
//        MtQueueLog mt = new MtQueueLog();
//        mt.setSdMoTxId((long) 117);
//        mt.setRegDate(new Date());
//        mt.setReceiveDate(new Date());
//        mt.setResponseTid("SENT|9240542618");
//        mt.setMsg("Password Anda adalah 066127. Anda dapat menggantinya pada menu akun Anda");
//        mt.setStatusCd("QS0003");
//        mt.setMtId((long) 120);
//        mt.setMsisdn("087776484466");
//        JdbcOracle ora = new JdbcOracle();
//        //ora.InsertLog(mt);
//        System.out.println(ora.getSeqMtQueue());
//        System.out.println(ora.getSeqMtQueueLog());
    }
}

